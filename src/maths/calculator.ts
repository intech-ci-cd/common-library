export class Calculator {
  public static sum(a: number, b: number): number {
    return a + b;
  }

  public static deduct(a: number, b: number): number {
    return a - b;
  }

  public static multiply(a: number, b: number): number {
    return a * b;
  }
}
