import { Calculator } from './calculator';

describe('maths', () => {
  it('1+1 should be equal to 2', () => {
    expect(Calculator.sum(1, 1)).toBe(2);
  });

  it('1-1 should be equal to 0', () => {
    expect(Calculator.deduct(1, 1)).toBe(0);
  });
});
